/**
 * Created by kay on 28.10.13.
 */
var util = require('util'),
    sys  = require('sys')
    ct = require('./cliTools.js')
    ;


module.exports = function (argv) {
    var paramChecker =  {
        checkParams : function (argv) {
            var foundedOptions = [];
            argv.forEach(function (val, index,arry) {
                console.log(index + ':' + val);
                //check for an argument
                if ( index >= 2) {
                    //explode the val
                    var result = paramChecker.checkModule(val);
                    //check the type from the result
                    if(typeof(result) === 'object') {
                        foundedOptions.push(result);
                    }
                }
            });

            if(foundedOptions.length >= 1) {
                return foundedOptions;
            }
        },
        /**
         * check for an specific options inside the params
         */
        checkModule : function (line) {
            //split it up!
            var cleanedLine = line.replace('-','');
            var splited = cleanedLine.split('=');
            if(splited.length >= 2) {
                //yes we found an pair of options
                return paramChecker.returnDetectedOption(splited);
            }
        },

        returnDetectedOption : function (optionsArray) {
            switch(optionsArray[0]) {
                case 'debug':
                    return paramChecker.returnCorrectBooleanType(optionsArray);
                    break;
                case 'copystatic':
                    return paramChecker.returnCorrectBooleanType(optionsArray);
                    break;
                case '?':
                    paramChecker.getHelp();
                    break;

            }

        },

        /**
         * this method returns the correct boolean type! Or the array
         * this is used to check boolean type correct in the argv command line options
         * array
         * TODO: optimize this method, because it returns soemtimes an array and sometimes boolean
         * this cant be optimized by the V8 maybe the performance will be lecked right here!
         */
        returnCorrectBooleanType: function (optionsArray) {
            var returnOptionsArray=paramChecker.checkBoolean(optionsArray);
            if(returnOptionsArray === null) {
                return false;
            } else {
                return optionsArray;
            }
        },
        /**
         * checks the commandline args value if there is an string boolean value in it
         * if there is an boolean value it will return the booleand value. If there isnt an boolean
         * value it will return null
         * @param optionsArray
         */
        checkBoolean: function (optionsArray) {
            if(optionsArray[1] === 'true' || optionsArray[1] === 'false') {
                optionsArray[1] = (optionsArray[1]==='true' ? true:false);
                return optionsArray;
            } else {
                return null;
            }
        },
        //if we found something courious inside the options get the user an small help
        getHelp : function () {
            var helpArr = [
                        '',
                        '',
                        '',
                        'istart build tool helper',
                        '(c) by Kay Schneider kayoliver82@gmail.com',
                        'http://istart.jspert.de   --> powerd by GAE',
                        '###########################################',
                        '',
                        'avaiable arguments:',
                        'debug              =       true debug mode on ',
                        ', false debug mode off by default set to false',
                        'copystatic         =       true (on), false(off)',
                        ' dont build something, copy only static value, by default set to false',
                        '',
                        '',
                        ''
            ];
            //clear the console
            ct.clear();

            helpArr.forEach(function(item) {
                sys.puts(item);
            })

            process.exit(1);
        }


    };
    return  paramChecker.checkParams(argv);
};
