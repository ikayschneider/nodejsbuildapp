/**
 * process for the jsFiles. Make all the jsFiles small and build it to one large
 * file
 *
 * make an randomized output name and save it to the buuild foleder
 *
 */

var path = require('path');
var fs = require('fs');
var yuicompressor = require('yuicompressor');
var readFiles = require('./readFiles.js');

var cssFiles = function () {
    this.files = [];
    this.packname= null;
    this.outFolder = null;
    this.tempFolder = null;
    this.fs =fs;
    this.tempName = '';
    this.compilerCall = 'java -jar ../../closure/compiler.jar --css %tempFile% --css_output_file %outputfile%';
};

cssFiles.prototype = new readFiles();
cssFiles.constructor = cssFiles;


cssFiles.prototype.init = function (files,packagename,globalOutFolder, tempFolder,debug) {
    this.debug=debug;
    this.files = files;
    this.packname = packagename;
    this.outFolder = globalOutFolder;
    this.tempFolder  = tempFolder;
    this.tempName = new Date().getTime() + this.packname + '.css';
    this.outName = new Date().getTime() + this.packname + '.min.css';
};

cssFiles.prototype.pushFiles = function (fileData,index) {
    this.buffer.push( {position:index,content:fileData} );
    this.itemsCount++;
    this.checkItemsReady();
};

cssFiles.prototype.compileFiles = function () {
    if(this.debug === false) {
        var that = this;
        yuicompressor.compress( this.tempFolder +  this.tempName, {
           charset:'utf-8',
           type: 'css'

        }, function (err,data,extra) {
               //console.log(err,data);
                var writeResult = fs.appendFileSync( 'output/js/' + that.outName,data);
                that.delTempFile();
        });
    } else {
        //do nothing
        this.delTempFile();
    }
};



cssFiles.prototype.getTag = function () {
    return '<link href="' + '../js/' +   this.getScriptFileName() + '" rel="stylesheet" type="text/css" />';
};

module.exports = cssFiles;