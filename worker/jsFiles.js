/**
 * process for the jsFiles. Make all the jsFiles small and build it to one large
 * file
 *
 * make an randomized output name and save it to the buuild foleder
 *
 */

var path = require('path');
var fs = require('fs');
var readFiles = require('./readFiles.js');

var jsFiles = function () {

};


jsFiles.prototype = new readFiles();
jsFiles.constructor = jsFiles;



jsFiles.prototype.init = function (files,packagename,globalOutFolder, tempFolder,debug) {
    this.debug = debug;
    this.files = [];
    this.packname= null;
    this.outFolder = null;
    this.tempFolder = null;

    this.fs =fs;
    this.tempName = '';
    this.compilerCall = 'java -jar ../../closure/compiler.jar  --js %tempFile% --js_output_file %outputfile%';

    this.files = files;
    this.packname = packagename;
    this.outFolder = globalOutFolder;
    this.tempFolder  = tempFolder;
    this.tempName = new Date().getTime() + this.packname + '.js';
    this.outName = new Date().getTime() + this.packname + '.min.js';
};

jsFiles.prototype.pushFiles = function (fileData,index) {
    /**
     * write all the files down to the out.js
     */
    //dont push the file immediatly.... add it at first to an buffer array
    this.buffer.push( {position:index,content:fileData} );
    this.itemsCount++;
    this.checkItemsReady();

};



jsFiles.prototype.getTag = function () {
     return '<script src="' + '../js/' +   this.getScriptFileName() + '" ></script>';
};


module.exports = jsFiles;