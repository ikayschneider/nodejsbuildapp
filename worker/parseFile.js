var baseObject = require('./baseObject');
var fs = require('fs');
var path = require('path');
var sys = require('sys');

var parseFile  = function() {

};


parseFile.prototype = new baseObject();
parseFile.constructor = parseFile;//use own constructor

/**
 *
 * @param fileName
 * @param folder
 * @param data
 */
parseFile.prototype.init = function(fileName, folder, data,outfolder) {
    this.template = fileName;
    this.folder = folder;
    this.dataObject = data;
    this.fileBuffer = null;
    this.outFolder = outfolder;
};

parseFile.prototype.readFileContents = function () {
    (function(that,filesFolder, filesFile) {
        fs.readFile('../istartnewtab/html/'  + filesFolder +  filesFile , {encoding:'utf8'} ,function(err, data, count) {
            that.pushFiles(data);
        });
    })(this,this.folder, this.template);
};


parseFile.prototype.pushFiles = function (filecontents){
    this.fileBuffer = filecontents;
    this.replaceContent();
};


parseFile.prototype.replaceContent = function () {
    for(var i in this.dataObject) {
        this.fileBuffer = this.fileBuffer.replace('<%'+this.dataObject[i].pack+'%>',this.dataObject[i].fileName);
    }
    this.writeOutFile();
};

parseFile.prototype.writeOutFile= function () {
    fs.writeFile(this.outFolder + this.template, this.fileBuffer, function (err) {
        if (err) throw err;
        console.log('It\'s saved!');
    });
};


module.exports = parseFile;
