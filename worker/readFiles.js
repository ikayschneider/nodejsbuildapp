/**
 * Created by kay on 14.10.13.
 */
var fs = require('fs');
var path = require('path');
var sys = require('sys');
var exec = require('child_process').exec;
var baseObject = require('./baseObject.js');



/**
 * read all the files to one file and make them smaller
  */
var readFiles = function () {
    this.largeStream = '';
    this.workersEmpty = false;//indicator so that we can test if the worker has all the work done
    this.allDone = false;
    this.itemsCount = 0;

};


readFiles.prototype = new baseObject();
readFiles.prototype.constructor = readFiles;



/**
 * read all the files in one file
 * read all the data down
 */
readFiles.prototype.start = function () {
    //init all attributes for the master object wich shouldnt be the same in it!
    this.buffer=[];
    sys.puts("start read files from fileSystem in package:" + this.packname);
    for(var item in this.files) {
        var count = item;
        //using here the advanced closure thechnics to avoid jum between the contexts

        (function(that,filesFolder, filesFile,fileIndex) {
            that.fs.readFile('../istartnewtab/html/'  + filesFolder +  filesFile , {encoding:'utf8'} ,function(err, data, count) {
                if(err !== null) {
                    process.send({message:'error',error:err,type:'kill'});
                }
                that.pushFiles(data,fileIndex);
            });
        })(this,this.files[item].folder, this.files[item].file, item);
    }
};

readFiles.prototype.appendFile = function (fileData) {

    var result = this.fs.appendFileSync('output/js/' + this.tempName , '\n' + fileData);
    console.log(result);
    return result;


};                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          

readFiles.prototype.checkItemsReady = function() {

    if(this.itemsCount == this.files.length) {
        this.startOutput();
    }
};

readFiles.prototype.startOutput = function () {
    var correctOrder = [];
    var position = 0;
    sys.puts("start write files from buffer:" + this.packname);
    for(var i=0;i<=this.buffer.length-1;i++) {
        correctOrder.push(this.searchPosition(i));
    }
    var writeBuffer ='';
    for(var item in correctOrder) {
        if(correctOrder[item] === undefined ||correctOrder[item] === 'undefined' ||correctOrder[item] === 'notFound') {
            process.send({message:'error',error: {message:correctOrder[item], index:item, pack:this.packname, file:this.files[item]},type:'kill'});
        }
       this.appendFile(correctOrder[item]);

       //var next= this.appendFile(correctOrder[item]);
    }



    this.compileFiles();
};

readFiles.prototype.searchPosition= function (search) {
    for(var i in this.buffer) {
        if(this.buffer[i].position == search) {
            return this.buffer[i].content;
        }


    }
    return "notFound";
};

readFiles.prototype.pushFiles = function (fileData, index) {
    console.error("your instance doesnt include the method pushFiles(fileData) please include this mehtod");
    //kill the process and quit
    process.kill();
};

/**
 * compiles the files
 */
readFiles.prototype.compileFiles = function () {
    if(this.debug === false) {
        var tempName = path.resolve( this.tempFolder + this.tempName) ;
        var outName  =path.resolve( this.tempFolder + this.outName);

        var call = this.compilerCall.replace('%tempFile%', tempName).replace('%outputfile%', outName);
        (function(that) {
            child = exec(call, function(error,stdout, stderr) {
                sys.print('stderr:' + stderr);
                that.delTempFile();
                if(error != null) {
                    console.log("exec error In FILE:" + error);
                    return 0;
                }
            })
        })(this);
    } else {
        this.delTempFile();
    }
};

/**
 * get the script name, check the curerent
 * @returns {jsFiles.outName|*}
 */
readFiles.prototype.getScriptFileName = function () {
    return (this.debug===false ? this.outName:this.tempName);
};

/**
 * delete the template files
 */
readFiles.prototype.delTempFile = function () {
    if(this.debug === false) {
         this.delFile( this.tempFolder + this.tempName );
        //this is call more than logocal expected! WHY!!!
         this.throwEvent('filesready',this.packname);
    } else {
        this.throwEvent('filesready', this.packname);
    }
};


readFiles.prototype.getOutName = function () {
    return this.outName;
};


readFiles.prototype.delFile = function(name) {
    fs.unlink(path.resolve(name));
};


module.exports = readFiles;